<?php


use Delighted\Client;
use Delighted\Person;
use Delighted\SurveyResponse;

class DelightedSurvey
{
    private $key = 'DELIGHTED_API_KEY';

    public function __construct()
    {
        Client::setApiKey($this->key);
    }

    public function createPerson($data)
    {
        try {
            $response = Person::create(
                [
                    'name' => $data['user']['name'],
                    'email' => $data['user']['email'],
                    'send' => 'false',
                    'locale' => 'pt-BR',
                    'properties' => [
                            'codigo_chamado' => $data['ticket'],
                            'codigo_cliente' => $data['customer']['id'],
                            'nome_cliente' => $data['customer']['name'],
                            'estrelas_cliente' => $data['customer']['nivel'],
                        ],
                ]
            );
        } catch (Exception $e) {
            // Log information catch
        }

        return $response;
    }

    public function sendSurveyResponse($data)
    {
        $data = array(
            'person' => $data['person']['id'],
            'score' => $data['score'],
            'comment' => $data['comment'],
            'person_properties' => [
                    'codigo_chamado' => $data['ticket'],
                    'codigo_cliente' => $data['customer']['id'],
                    'nome_cliente' => $data['customer']['name'],
                    'estrelas_cliente' => $data['customer']['nivel'],
                ],
        );

        try {
            $response = SurveyResponse::create($data);
        } catch (Exception $e) {
            // Log information catch
        }

        return $response;
    }
}
